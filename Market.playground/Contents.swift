//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


enum offers: Float {
    case Twenty = 0.20
    case Thirty = 0.30
    case Fifty = 0.50
}

enum Categories: String {
    case Food = "Food"
    case Clothings = "Clothings"
    case Groceries = "Groceries"
}


class Products {
    var pName : String
    var pId : Int
    var price : Double
    var category : Categories
    var qty : Double
    var poffer : offers
    init(pName :String, price : Double, qty: Double, pId: Int, productOffer: Float, prodCategory: String)
    {
        self.pName = pName
        self.price = price
        self.qty = qty
        self.pId = pId
        self.poffer = offers(rawValue:productOffer)!
        self.category = Categories(rawValue:prodCategory)!
    }
}
class SuperMarket {
    var products = [Products]()
    var customerProducts = [Products] ()
    var productDict = [Int : Double]()
    
    func addProductsToDatabase(product:Products){
        
        products.append(product)
        
    }
    
    func addProductToCart(productId: Int, qty : Double) {
        if productId > 0 {
            for product in products {
                if (product.pId == productId && product.qty >= qty){
                    customerProducts.append(product)
                    productDict = [product.pId : qty]
                }
                /*else {
                 print ("Item not found")
                 }*/
            }
        }
        else{
            print("Invalid Item")
        }
    }
    
    
    func billGenerator()
    {  var totalBillAmount = 0.0
        for product in customerProducts {
            if product.qty > 0{
                totalBillAmount = totalBillAmount + priceCalculator (product.pId, price: product.price)
            }
            print("Total Bill Amount is \(totalBillAmount)")
        }
    }
    func printTypeValues (type : String){
        
    }
    
    func priceCalculator(pId : Int, price: Double) -> Double
    {
        for qty in productDict.values {
            if qty > 1 {
                var finalPrice = price * qty
                return finalPrice
            }
            else if qty == 1{
                return price
            }
        }
        return 0
    }
}

var add = SuperMarket()
let product1 = Products(pName :"maggi", price : 25.0 ,qty : 5, pId : 127, productOffer : 0.20, prodCategory:"Food")
add.addProductsToDatabase(product1)
let product2 = Products(pName :"tea powder", price : 20.0 ,qty : 5, pId : 101, productOffer : 0.20, prodCategory:"Clothings")
add.addProductsToDatabase(product2)
add.addProductToCart(101, qty: 5)
//add.addProductToCart(101, qty: 2)
add.billGenerator()