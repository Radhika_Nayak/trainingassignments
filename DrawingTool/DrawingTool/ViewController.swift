//
//  ViewController.swift
//  DrawingTool
//
//  Created by Radhika on 8/18/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let simplePath = UIBezierPath()
    var interpolationPoints = [CGPoint]()
    let backgroundLayer = CAShapeLayer()
    let drawingLayer = CAShapeLayer()
    var lastPoint = CGPoint.zero
    var swiped = false
    
    
    @IBOutlet weak var displayImage: UIImageView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        backgroundLayer.strokeColor = UIColor.darkGrayColor().CGColor
        backgroundLayer.fillColor = nil
        backgroundLayer.lineWidth = 2
        drawingLayer.strokeColor = UIColor.blackColor().CGColor
        drawingLayer.fillColor = nil
        drawingLayer.lineWidth = 2
        displayImage.layer.addSublayer(backgroundLayer)
        displayImage.layer.addSublayer(drawingLayer)

        
}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?){
        swiped = false
         if let touch = touches.first{
            lastPoint = touch.locationInView(self.view)
            simplePath.moveToPoint(lastPoint)
            interpolationPoints = [lastPoint]                 //populate the interpolationpoints array
          
        }
}
    
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
         swiped =  true
         if let touch = touches.first, coalescedTouches = event?.coalescedTouchesForTouch(touch) {
            let currentPoint = touch.locationInView(view)
        
            for coalescedTouch in coalescedTouches {             // reduce lagging
            
                interpolationPoints.append(currentPoint)
                simplePath.removeAllPoints()
                simplePath.interpolationPoints(interpolationPoints, closed: false, alpha: 1)
                
                drawingLayer.path = simplePath.CGPath
                
            }
        }
        
      
            
    
        
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?){
//        
//          if  !swiped{
//            //simplePath.removeAllPoints()
//            print(interpolationPoints[0])
//            simplePath.interpolatePoints(interpolationPoints, alpha: 0.8 )
//            drawingLayer.path = simplePath.CGPath
//        }
//        
        
       if let backgroundPath = backgroundLayer.path
       {
           simplePath.appendPath(UIBezierPath(CGPath: backgroundPath))
        }
        
           backgroundLayer.path = simplePath.CGPath
       
           //simplePath.removeAllPoints()
       
            drawingLayer.path = simplePath.CGPath

        
    }
    
    
//    func drawLine(fromPoint : CGPoint, toPoint :CGPoint){
//        
//        simplePath.moveToPoint(fromPoint)
//        simplePath.addLineToPoint(toPoint)
//        drawingLayer.path = simplePath.CGPath
//        
//        
//        if let backgroundPath = backgroundLayer.path
//        {
//            simplePath.appendPath(UIBezierPath(CGPath: backgroundPath))
//        }
//        
//        backgroundLayer.path = simplePath.CGPath
//        
//        simplePath.removeAllPoints()
//        
//        drawingLayer.path = simplePath.CGPath
//        
//    }
}
    









//
//
//extension UIBezierPath {
//    
//    func interpolateCGPointsWithCatmullRom(points : [CGPoint], closed : Bool, alpha : Float) {
//      
//        
//            
//            guard points.count > 3 else { return }
//            
//            assert(alpha >= 0 && alpha <= 1.0, "Alpha must be between 0 and 1")
//        
//            let endIndex = closed ? points.count : points.count - 2
//            
//            let startIndex = closed ? 0 : 1
//            
//            let kEPSILON: Float = 1.0e-5
//            
//            moveToPoint(points[startIndex])
//            
//            for index in startIndex ..< endIndex {
//                let nextIndex = (index + 1) % points.count
//                let nextNextIndex = (nextIndex + 1) % points.count
//                let previousIndex = index < 1 ? points.count - 1 : index - 1
//                
//                let point0 = points[previousIndex]
//                let point1 = points[index]
//                let point2 = points[nextIndex]
//                let point3 = points[nextNextIndex]
//                
//                let d1 = hypot(Float(point1.x - point0.x), Float(point1.y - point0.y))
//                let d2 = hypot(Float(point2.x - point1.x), Float(point2.y - point1.y))
//                let d3 = hypot(Float(point3.x - point2.x), Float(point3.y - point2.y))
//                
//                let d1a2 = powf(d1, alpha * 2)
//                let d1a  = powf(d1, alpha)
//                let d2a2 = powf(d2, alpha * 2)
//                let d2a  = powf(d2, alpha)
//                let d3a2 = powf(d3, alpha * 2)
//                let d3a  = powf(d3, alpha)
//                
//                var controlPoint1: CGPoint, controlPoint2: CGPoint
//                
//                if fabs(d1) < kEPSILON {
//                    controlPoint1 = point2
//                } else {
//                    
//                    var value1 =  (3 * d1a * (d1a + d2a))
//                    
//                    var value2 =  (2 * d1a2 + 3 * d1a * d2a + d2a2)
//                    
//                    controlPoint1 = (point2 * d1a2 - point0 * d2a2 + point1 * value2 ) / value1
//                }
//                if fabs(d3) < kEPSILON {
//                    controlPoint2 = point2
//                } else {
//                    controlPoint2 = (point1 * d3a2 - point3 * d2a2 + point2 * (2 * d3a2 + 3 * d3a * d2a + d2a2)) / (3 * d3a * (d3a + d2a))
//                }
//                
//                addCurveToPoint(point2, controlPoint1: controlPoint1, controlPoint2: controlPoint2)
//            }
//            
//            if closed { closePath() }
//        }
//        
//    }

