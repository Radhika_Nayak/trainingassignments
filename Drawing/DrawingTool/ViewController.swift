//
//  ViewController.swift
//  DrawingTool
//
//  Created by Radhika on 8/16/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var diaplayImage: UIImageView!
    
    var lastPoint = CGPoint.zero
    var red: CGFloat = 0.0
    var green: CGFloat = 0.0
    var blue: CGFloat = 0.0
    var brushWidth: CGFloat = 2.0
    var opacity: CGFloat = 1.0
    var swiped = false
    
    
    let colors: [(CGFloat, CGFloat, CGFloat)] = [
        (0, 0, 0),
        (105.0 / 255.0, 105.0 / 255.0, 105.0 / 255.0),
        (1.0, 0, 0),
        (0, 0, 1.0),
        (51.0 / 255.0, 204.0 / 255.0, 1.0),
        (102.0 / 255.0, 204.0 / 255.0, 0),
        (102.0 / 255.0, 1.0, 0),
        (160.0 / 255.0, 82.0 / 255.0, 45.0 / 255.0),
        (1.0, 102.0 / 255.0, 0),
        (1.0, 1.0, 0),
        (1.0, 1.0, 1.0),
        ]
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func pencilPressed(sender: AnyObject) {
        
        (red, green, blue) = colors[0]
        
    }
    
    
    @IBAction func clearPressed(sender: AnyObject) {
        
        diaplayImage.image = nil
    }
    
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        swiped = false
        if let touch = touches.first{
            lastPoint = touch.locationInView(self.view)
        }
    }
    
    
    
    func drawLineFrom(fromPoint: CGPoint, toPoint: CGPoint) {
        
        UIGraphicsBeginImageContext(diaplayImage.frame.size)
        let context = UIGraphicsGetCurrentContext()
        diaplayImage.image?.drawInRect(CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        CGContextMoveToPoint(context, fromPoint.x, fromPoint.y)
        CGContextAddLineToPoint(context, toPoint.x, toPoint.y)
         CGContextSetLineCap(context, CGLineCap.Round)
        CGContextSetLineWidth(context, brushWidth)
      
        CGContextSetRGBStrokeColor(context, red, green, blue, 1.0)
      CGContextSetBlendMode(context, CGBlendMode.Normal)
      //  CGContextSetLineCap(UIGraphicsGetCurrentContext(),CGLineCap.Round)
        CGContextStrokePath(context)
        diaplayImage.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
    }
    
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        swiped = true
        if let touch = touches.first {
            let currentPoint = touch.locationInView(view)
            drawLineFrom(lastPoint, toPoint: currentPoint)
            lastPoint = currentPoint
        }
        
}
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        // to draw a single point
        if !swiped {
            drawLineFrom(lastPoint, toPoint: lastPoint)
        }
        
    }
    
    
}
