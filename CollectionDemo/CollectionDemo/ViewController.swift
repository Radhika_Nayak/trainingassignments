//
//  ViewController.swift
//  CollectionDemo
//
//  Created by Radhika on 8/8/16.
//  Copyright © 2016 Radhika. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

   // @IBOutlet weak var collectionView: UICollectionViewCell!
    var collectionView1: UICollectionView!

    
    
    
  //  let identifier = "CellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 50, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 100, height: 100)
        
        
        
        
        collectionView1 = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        
        collectionView1.dataSource = self
        collectionView1.delegate = self
          collectionView1.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView1.backgroundColor = UIColor.whiteColor()
      //  collectionView1.ba
        self.view.addSubview(collectionView1)
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        cell.backgroundColor = UIColor.orangeColor()
    
        return cell
    }
    
    

   override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    


}