//
//  Images+CoreDataProperties.swift
//  PaintBoard
//
//  Created by Radhika on 8/30/16.
//  Copyright © 2016 yml. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Images {

    @NSManaged var name: String?
    @NSManaged var image: NSData?

}
