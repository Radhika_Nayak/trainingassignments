//
//  DisplayImages.swift
//  PaintBoard
//
//  Created by Radhika on 8/31/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit
import CoreData

protocol   TableViewImageDelegate {                 // protocol declaration
    func didSelectImage(image : UIImage, name : String, isExist : Bool)
}

class DisplayImagesViewController: UITableViewController {
    var imageList = [Images]()
    var tempImages = [Images]()
    var delegate: TableViewImageDelegate?
    
    
//MARK: - ViewLifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
     
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.fetchData()
        tableView.reloadData()
    }
    

// MARK: - Table view data source
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return imageList.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        let imageCount = imageList[indexPath.row]
        let title = imageCount.valueForKey("name") as! String
        cell.textLabel!.text = title
        return cell
    }
   
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(indexPath.row)
        let imageCount = imageList[indexPath.row] as Images
        if let del = delegate {
            let imageSelect : UIImage = imageCount.valueForKey("image") as! UIImage
            let imageName :String = imageCount.valueForKey("name") as! String
            del.didSelectImage(imageSelect, name:imageName,isExist: true)
        }
    }

// MARK: - fetchData
    func fetchData(){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Images")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            imageList = results as! [Images]
        }catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
     }
}
