//
//  ViewController.swift
//  Demo
//
//  Created by Radhika on 8/17/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit
import CoreData


class HomeViewController: UIViewController, UIPopoverPresentationControllerDelegate, TableViewColorDelegate,  TableViewImageDelegate {
    
    @IBOutlet weak var drawingContainer: UIView!
    var signatureView : DrawView?
    var drawImage : UIImage?
    var names = [String]()
    var imageList = [NSManagedObject]()
    var tempname : String!
    
    
// MARK: - View Life Cycle Methods
    override func viewDidLoad() {
         super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        signatureView = DrawView(frame: CGRectMake(0, 0, drawingContainer.bounds.size.width,  drawingContainer.bounds.size.height))
        drawingContainer.addSubview(signatureView!)
    }
    

// MARK : -  Action Methods
    @IBAction func tapClearCanvas(sender: AnyObject) {
        
        signatureView?.cleanCanvas()
    }
    
    @IBAction func tapShare(sender: AnyObject) {
        if (signatureView?.incrImage==nil) {     // if image is nil
            let alertController = UIAlertController(title: "Save Image", message: "Its empty!", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }else{
            let activityViewController = UIActivityViewController(activityItems: [(signatureView?.incrImage)!], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = sender as! UIButton
            activityViewController.popoverPresentationController?.sourceRect = (sender as! UIButton).bounds
            activityViewController.excludedActivityTypes = [ UIActivityTypeAirDrop, UIActivityTypePostToFacebook ]
            // present the view controller
            self.presentViewController(activityViewController, animated: true, completion: nil)
            UIGraphicsEndImageContext()
        }
    }

    @IBAction func eraserPressed(sender: AnyObject) {
        let  eraser  = sender as! UIButton
        if(!((signatureView?.eraser)!)){
        signatureView?.eraser = true
        eraser.selected = true
        }else {
        signatureView?.eraser = false
        eraser.selected = false
        }
    }
    
    @IBAction func backgroundColorPressed(sender: AnyObject) {
        let tableViewController = DropdownViewController()
        tableViewController.delegate = self
        tableViewController.modalPresentationStyle = UIModalPresentationStyle.Popover
        tableViewController.preferredContentSize = CGSizeMake(200, 200)
        presentViewController(tableViewController, animated: true, completion: nil)
        let popoverPresentationController = tableViewController.popoverPresentationController
        popoverPresentationController?.sourceView = sender as! UIButton
        popoverPresentationController?.sourceRect = CGRectMake(0, 0, sender.frame.size.width, sender.frame.size.height)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let settingsViewController = segue.destinationViewController as! SettingsViewController
        settingsViewController.delegate = self
        settingsViewController.brush = (signatureView?.brushWidth)!
        settingsViewController.opacity = (signatureView?.opacity)!
        settingsViewController.red = (signatureView?.red)!
        settingsViewController.green = (signatureView?.green)!
        settingsViewController.blue = (signatureView?.blue)!
    }
    
       // save the image
    @IBAction func didTapSave(sender: UIButton) {
        if !signatureView!.isExist {     // if the image is new
            let alert = UIAlertController(title: "File Name", message: "Add a new name", preferredStyle: .Alert)
            let saveAction = UIAlertAction(title: "Save", style: .Default, handler: { (action:UIAlertAction) -> Void in
            let textField = alert.textFields!.first
            self.saveImage (textField!.text!, image: self.signatureView!.incrImage!)})
            let cancelAction = UIAlertAction(title: "Cancel", style: .Default) { (action: UIAlertAction) -> Void in}
            alert.addTextFieldWithConfigurationHandler {(textField: UITextField) -> Void in }
            alert.addAction(saveAction)
            alert.addAction(cancelAction)
            presentViewController(alert, animated: true, completion: nil)
        } else {
                signatureView?.isExist = false    // if image is already existing
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                let managedContext = appDelegate.managedObjectContext
                let fetchRequest = NSFetchRequest(entityName: "Images")
                fetchRequest.predicate = NSPredicate(format: "name = %@", tempname)
                do{
                    if let fetchResults =  try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
                    if fetchResults.count != 0 {
                    let managedObject = fetchResults[0]
                    managedObject.setValue(signatureView?.incrImage, forKey: "image")
                    do {
                        try managedContext.save()
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                    }
                  }
                } catch let error as NSError {
                    print("Could not fetch \(error), \(error.userInfo)")
                }
        }
            
    }
 //tavble view listing the saved images
    @IBAction func displayStoredImages(sender: UIButton) {
        let tableViewController = DisplayImagesViewController()
        tableViewController.delegate = self
        tableViewController.modalPresentationStyle = UIModalPresentationStyle.Popover
        presentViewController(tableViewController, animated: true, completion: nil)
        let popoverPresentationController = tableViewController.popoverPresentationController
        popoverPresentationController?.sourceView = sender 
        popoverPresentationController?.sourceRect = CGRectMake(0, 0, sender.frame.size.width, sender.frame.size.height)

    }


 // MARK :  Save Method
    func saveImage(name: String, image : UIImage) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let entity =  NSEntityDescription.entityForName("Images", inManagedObjectContext:managedContext)
        let imageId = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
        imageId.setValue(name, forKey: "name")
        imageId.setValue(image, forKey: "image")
        print(name)
        print(image)
        do {
            try managedContext.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    

// MARK: - TableViewColorMethods
    func didSelectBackgroundColor(r: CGFloat, g: CGFloat, b:CGFloat)
    {
        signatureView?.initialBG = 1
        signatureView?.bgColor = UIColor(red: r/255, green : g/255, blue: b/255, alpha: 1)
        signatureView?.backgroundColor = UIColor(red: r/255, green : g/255, blue: b/255, alpha: 1)
        

    }
   
// MARK: - TableViewImageMethods
    func didSelectImage(image : UIImage, name :String , isExist :Bool){
        signatureView?.incrImage = image
        tempname = name
        signatureView?.isExist = isExist
        signatureView?.drawBitmapImage()
        signatureView?.setNeedsDisplay()
    }
}

// MARK: -  SettingsViewControlDelegate
extension HomeViewController: SettingsViewControllerDelegate {
    func settingsViewControllerFinished(settingsViewController: SettingsViewController) {
        signatureView!.brushWidth = settingsViewController.brush
        signatureView!.opacity = settingsViewController.opacity
        signatureView!.red = settingsViewController.red
        signatureView!.green = settingsViewController.green
        signatureView!.blue = settingsViewController.blue
 }
}