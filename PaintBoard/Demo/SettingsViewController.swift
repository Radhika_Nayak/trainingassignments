//
//  SettingsViewController.swift
//  Demo
//
//  Created by Radhika on 8/24/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit


protocol SettingsViewControllerDelegate {
    func settingsViewControllerFinished(settingsViewController: SettingsViewController)
}

class SettingsViewController: UIViewController {
    @IBOutlet weak var sliderBrush: UISlider!
    @IBOutlet weak var sliderOpacity: UISlider!
    @IBOutlet weak var sliderRed: UISlider!
    @IBOutlet weak var sliderGreen: UISlider!
    @IBOutlet weak var sliderBlue: UISlider!
    @IBOutlet weak var labelBrush: UILabel!
    @IBOutlet weak var labelOpacity: UILabel!
    @IBOutlet weak var labelRed: UILabel!
    @IBOutlet weak var labelGreen: UILabel!
    @IBOutlet weak var labelBlue: UILabel!
    @IBOutlet weak var imageOpacity: UIImageView!
    var brush: CGFloat = 20.0
    var opacity: CGFloat = 1.0
    var red: CGFloat = 0.0
    var green: CGFloat = 0.0
    var blue: CGFloat = 0.0
    var delegate: SettingsViewControllerDelegate?
    
    
// MARK: - View life Cylcle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        sliderBrush.minimumValue = 0
        sliderBrush.maximumValue = 50
        sliderOpacity.minimumValue = 0
        sliderOpacity.maximumValue = 1
        sliderRed.minimumValue = 0
        sliderRed.maximumValue = 255
        sliderGreen.minimumValue = 0
        sliderGreen.maximumValue = 255
        sliderBlue.minimumValue = 0
        sliderBlue.maximumValue = 255
        reflectChangeSettings()
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Slider Value Changed
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        sliderBrush.value = Float(brush)
        labelBrush.text = "\(Int(sliderBrush.value))"
        sliderOpacity.value = Float(opacity)
        labelOpacity.text = "\(Int(sliderOpacity.value))"
        sliderRed.value = Float(red )
        labelRed.text = "\(Int(sliderRed.value))"
        sliderGreen.value = Float(green)
        labelGreen.text = "\(Int(sliderGreen.value))"
        sliderBlue.value = Float(blue)
        labelBlue.text = "\(Int(sliderBlue.value))"
    }
    
// MARK: - Handling Button Actions
    
    @IBAction func close(sender: AnyObject){
        dismissViewControllerAnimated(true, completion: nil)
        self.delegate?.settingsViewControllerFinished(self)
    }

    @IBAction func sliderChange(sender: UISlider) {
        if sender == sliderBrush {
            brush =   CGFloat(sender.value)
            labelBrush.text = "\(Int(brush))"
        } else {
            opacity = CGFloat(sender.value)
            labelOpacity.text = "\(opacity)"
        }
         reflectChangeSettings()
        
    }

    @IBAction func colorChange(sender: UISlider) {
        red = CGFloat(sliderRed.value)
        labelRed.text = "\(Int(red))"
        green = CGFloat(sliderGreen.value)
        labelGreen.text = "\(Int(green))"
        blue = CGFloat(sliderBlue.value)
        labelBlue.text = "\(Int(blue))"
        reflectChangeSettings()
    }
 
// MARK: -  ImageView Methods
    func reflectChangeSettings() {
        UIGraphicsBeginImageContext(imageOpacity.frame.size)
        let  context = UIGraphicsGetCurrentContext()
        CGContextSetLineCap(context, CGLineCap.Round)
        CGContextSetLineWidth(context, 20)
        CGContextMoveToPoint(context, 45.0, 45.0)
        CGContextAddLineToPoint(context, 45.0, 45.0)
        CGContextSetRGBStrokeColor(context, red/255, green/255, blue/255, opacity)
        CGContextStrokePath(context)
        imageOpacity.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
}
