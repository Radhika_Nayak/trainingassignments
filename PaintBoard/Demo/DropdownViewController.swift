//
//  DropdownViewController.swift
//  Demo
//
//  Created by Radhika on 8/23/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit

protocol TableViewColorDelegate {                 // protocol declaration
  func didSelectBackgroundColor(r: CGFloat, g: CGFloat, b:CGFloat)
}


class DropdownViewController: UITableViewController{
    var delegate: TableViewColorDelegate?
    var items = ["Red", "Green","Blue","White"]
    var red : CGFloat = 0.0
    var green: CGFloat = 0.0
    var blue : CGFloat =  0.0
    let colors: [(CGFloat, CGFloat, CGFloat)] = [ (255, 0, 0), (0, 255, 0), (0, 0, 255),(255,255,255)]

  
    // MARK: - Viewcylcle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "LabelCell")
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

   // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

     
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "LabelCell")
        cell.textLabel!.text = items [indexPath.row]
        return cell;
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(indexPath.row)
        if let del = delegate {
            (red, green, blue) = colors[indexPath.row]
            del.didSelectBackgroundColor(red, g: green, b: blue)
        }
       }
     }
