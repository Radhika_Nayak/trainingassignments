//
//  DrawView.swift
//  Demo
//
//  Created by Radhika on 8/19/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit

class DrawView : UIView{
    
    var beizerPath = UIBezierPath()
    var incrImage:UIImage?
    var points: [CGPoint] = Array<CGPoint>(count: 5, repeatedValue: CGPointZero)
    var control = 0
    var red: CGFloat = 0.0
    var green = CGFloat(0.0)
    var blue: CGFloat = 0.0
    var brushWidth:CGFloat = 2.0
    var opacity:CGFloat = 1.0
    var eraser  = false
    var swiped = false
    var colorcode = 0
    var bgColor =  UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
    var initialBG = 0
    var isExist = false
    var undo = 0
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // All classes should follow this rules
        // TODO: move draw logic VC
        // TODO: remove extraspace in between method definition,Maintain single line space between method
        // TODO: use meaningful name for methods, like, pencilColorChange() etc
        // TODO: follow type inferencing i.e, no need to declare type when setting types for var's
        // TODO: remove commended out/ unwanted code
        // TODO: ViewControllers naming should be differentiated from views as a example ,ViewController at the end
        // TODO: protocals should pass self instance bydefault
        // TODO: simplify lines of code in a single method by creating small methods if needed
        // TODO: Naming of Variables,Functions,Classes should be meaningful form
        // TODO: Write comments on top of every method to illustrate its purpose
        // TODO: uncomment super call for statndard life cycle methods to avoid inconsistances
        // TODO: isolate dataModel into a different class
        // TODO: Change delegate and Datasource methods as extentions for specific class
        // TODO: Remove all warnings and layout warnings
        // TODO: Group Methods by nature (i.e, add Pragma for group os methods)
    
        self.backgroundColor = UIColor.whiteColor()
    }
    
    // MARK: -  Drawing
    // override drawRect with custom drawing
    override func drawRect(rect: CGRect) {
        switch (undo) {
        case  1 :
            incrImage?.drawInRect(rect)
            beizerPath.lineWidth = brushWidth
            pencilColorChange()
            beizerPath.stroke()
            undo = 0
        default :
        incrImage?.drawInRect(rect)
        beizerPath.lineWidth = brushWidth
        pencilColorChange()
        beizerPath.stroke()
        }
    }
    

  // MARK: - Touch handling Methods
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {       // start drawing
        
        beizerPath.removeAllPoints()
        backgroundColorChange()
        swiped = false
        control = 0;
        let touch = touches.first!
        points[0] = touch.locationInView(self)
    }
 
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {       // during swiping
        swiped = true
        let touch = touches.first!
        let touchPoint = touch.locationInView(self)
        control += 1;
        points[control] = touchPoint;
        if (control == 4)
        {
            points[3] = CGPointMake((points[2].x + points[4].x)/2.0, (points[2].y + points[4].y)/2.0);
            beizerPath.moveToPoint(points[0])
            beizerPath.addCurveToPoint(points[3], controlPoint1: points[1], controlPoint2: points[2])
            self.setNeedsDisplay()
            points[0] = points[3];
            points[1] = points[4];
            control = 1;
        }
    }
    
    override  func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {      // called when user lift his finger off the screen
     // draw a single dot
       if  !swiped{
            beizerPath.lineWidth=brushWidth
            beizerPath.moveToPoint(points[0])
            beizerPath.addLineToPoint(points[0])
            beizerPath.lineCapStyle = .Round
            beizerPath.lineJoinStyle = .Round
            beizerPath.lineWidth = brushWidth
            pencilColorChange()
        }
        
        self.drawBitmapImage()
        self.setNeedsDisplay()
      //  beizerPath.removeAllPoints()
   }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?)  {
        self.touchesEnded(touches!, withEvent: event)
    }
    
    
  //MARK: -  Draw into UIImage

    func drawBitmapImage() {
            UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0);
            let rectpath = UIBezierPath(rect: self.bounds)
                backgroundColorChange()
            rectpath.fill()
            incrImage?.drawAtPoint(CGPointZero)
            pencilColorChange()
            beizerPath.stroke()
            incrImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
    }
    
  // MARK: - cleanCanvas
    
    func cleanCanvas() {
        beizerPath.removeAllPoints()
        beizerPath = UIBezierPath()
        backgroundColorChange()
        incrImage = nil
        self.setNeedsDisplay()
    }
    
    
 // MARK: -  pencil colorChange
     func pencilColorChange(){
        if !eraser {
            let color : UIColor = UIColor(red: red/255, green: green/255, blue: blue/255, alpha: opacity)
            color.setStroke()
            color.setFill()
        } else {
            let color : UIColor = bgColor     // changes the eraser color according to background color

            color.setStroke()
            color.setFill()
        }
    }
    

    
// MARK: - backgroungColorChange
    
    func  backgroundColorChange() {
        if(initialBG==0){
            UIColor.clearColor().setFill()
        }else {
            let color : UIColor = bgColor
            self.backgroundColor = bgColor
            color.setFill()
        }
     }
  }


