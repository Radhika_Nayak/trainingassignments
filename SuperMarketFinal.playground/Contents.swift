//: Playground - noun: a place where people can play

import UIKit



enum prodtype :String {
    case Food = "Food"
    case Tools = "Tools"
    case Bags = "Bags"
}


class Product {
var productId : Int
var productName :String
var productPrice :Double
var productCount : Double
var productDiscount : Double
var productType:prodtype

init (productId : Int,productName :String,productPrice:Double,productCount:Double,productDiscount:Double,protype:String){
    self.productId=productId
    self.productName=productName
    self.productPrice=productPrice
    self.productCount=productCount
    self.productDiscount=productDiscount
    self.productType=prodtype(rawValue: protype)!
}
    
    func describe() -> String{
        return "\(self.productId)   \(self.productName)  \(self.productCount)  \(self.productPrice) \(self.productType)"
    }

}



class   CustomerProducts {
    var id:Int=0
    var name :String
    var phno:Int
    var buyproduct:[Product]
    
    init(id:Int,name:String,phno:Int,buyproduct:[Product]){
        self.name=name
        self.id=id
        self.phno=phno
        self.buyproduct=buyproduct
    }
    
    

    
    func bill() -> Double{
       var amount : Double = 0.0
        
        
       for pro in buyproduct {
        
       amount += ((pro.productCount * pro.productPrice) - ((pro.productCount * pro.productPrice) * (pro.productDiscount / 100 )))
            
        }
       return amount
        
    }
    
    
    
    
}
    
    




class Supermarket {
    
    
    var products = [Product]()
    var customers=[CustomerProducts]()
    
   func addProduct(product:Product) //add the product
    
    {
        
        products.append(product)
        
    }
    
    
    
    func deleteProductById(deleteId :Int) -> Bool //delete product by Id
    {
        var i:Int=0
        
        for product in products{
            if product.productId==deleteId {
                print("deleted product")
                products.removeAtIndex(i)
                
              
                return false
            }
            else { i=i+1 }
        }
        
        return true
    }
    
    
    func printAll()    //print  all products
    {
       var  productlist : String="\0"
        print("Product list")
        for pro in products{
            
            if searchProductById(pro.productId){
            
            productlist = productlist + pro.describe()+"\n"
            print(" \(pro.productId) \(pro.productName) \(pro.productCount) \(pro.productPrice) \(pro.productType)")
            }else {
             print ("not found")
            }
        }
        
    }
    
    
    func printProductType(type : String){   //print products based on type
        
        print("Product of type  " + "\(type)")
        
        for pro in products {
            
            if   String (pro.productType)==type {
                
                print(" \(pro.productId) \(pro.productName) \(pro.productCount) \(pro.productPrice)")
            }
        }
        
    }
    
    
    func searchProductById(key : Int) -> Bool //search  product
    {
        
      //  print("searched product")
        for prod in products
        {
            if prod.productId==key{
                
                return true
            }
        }
        return false
        
        
    }
    
    
    
    func  addCustomerProducts(cust:CustomerProducts) -> Bool{
        
        var amount: Double=0
        
        for pro in cust.buyproduct {
             var result : Bool = false
            for prod in products {
            
              if prod.productId==pro.productId {
                
                if(pro.productCount<=prod.productCount){
                result = true
                break
                }
            }
            
            }
            
           if !result{
                print ("invalid product added")
                return false
           }else {
            
            
            
            }
        }
        
        
        
        
        
            
        customers.append(cust)
        
        amount = cust.bill()
        print ("CustomerName : \(cust.name)   total amount  : \(amount)")
        return true
       
    }
    
    
 /* func editProductById(key:Int,type: String, value:String)
    {
        var result:BooleanType=true
        for pro in products {
            if (pro.productId)==key{
                result=false
                
                
            }
        }
        if result {
            print ("not found")
        }
        
    }
    */
   
    
    
    
    
    }



    
let supermarket = Supermarket()
var result : Bool = false

var productA = Product(productId: 1, productName: "A", productPrice: 50, productCount: 2, productDiscount: 20, protype: "Food")  // add the products
supermarket.addProduct(productA)



var productB = Product(productId: 2, productName: "B", productPrice: 100, productCount: 2, productDiscount: 10, protype: "Food")
supermarket.addProduct(productB)



var productC = Product(productId: 3, productName: "C", productPrice: 200, productCount: 2, productDiscount: 0, protype: "Tools")
supermarket.addProduct(productC)




supermarket.printAll()  // print the product list


result=supermarket.deleteProductById(4)   // delete product by Id

if result {
    print("deleteid not found")
}




supermarket.printAll()



supermarket.printProductType("Tools")  // print by product type


var custA = CustomerProducts(id: 1, name: "abc", phno: 12344, buyproduct: [productA,productB])  // add customerproducts

supermarket.addCustomerProducts(custA)


var productD = Product(productId: 10, productName: "B", productPrice: 100, productCount: 2, productDiscount: 10, protype: "Food") //product not added to product list

var custB = CustomerProducts(id: 1, name: "abc", phno: 12344, buyproduct: [productA,productD])  // fails to add customerproducts

supermarket.addCustomerProducts(custB)




