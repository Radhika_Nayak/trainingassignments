//
//  DrawView.swift
//  Demo
//
//  Created by Radhika on 8/19/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit

class DrawView : UIView{
    
    var beizerPath  : UIBezierPath = UIBezierPath()
    var incrImage   : UIImage?
    var points      : [CGPoint] = Array<CGPoint>(count: 5, repeatedValue: CGPointZero)
    var control     : Int = 0
    var red :  CGFloat = 0.0
    var green: CGFloat = 0.0
    var blue: CGFloat = 0.0
    var brushWidth :CGFloat = 2.0
    var opacity: CGFloat = 1.0
    var eraser : Bool = false
    var swiped : Bool = false
    var colorcode: Int = 0
    var bgColor : UIColor =  UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
    var initialBG = 0
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
  override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
    }
    
    // MARK: - drawRect Method
    // override drawRect with custom drawing
  override func drawRect(rect: CGRect) {
        incrImage?.drawInRect(rect)
        beizerPath.lineWidth = brushWidth
        colorChange()
        beizerPath.stroke()
    }
    
    
    
  // MARK: - Touch handling Methods
    
     override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {          // start drawing
        backgroundColorChange()
        swiped = false
        control = 0;
        let touch = touches.first!
        points[0] = touch.locationInView(self)
       // let startPoint = points[0];
        
    }
    
    
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {       // during swiping
        swiped = true
        let touch = touches.first!
        let touchPoint = touch.locationInView(self)
        control += 1;
        points[control] = touchPoint;
        if (control == 4)
        {
            points[3] = CGPointMake((points[2].x + points[4].x)/2.0, (points[2].y + points[4].y)/2.0);
            beizerPath.moveToPoint(points[0])
            beizerPath.addCurveToPoint(points[3], controlPoint1: points[1], controlPoint2: points[2])
            self.setNeedsDisplay()
            points[0] = points[3];
            points[1] = points[4];
            control = 1;
            
        }
    }
    
    
    
    
    override  func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {      // called when user lift his finger off the screen
        
        
        // draw a single dot
       if  !swiped{
            beizerPath.lineWidth=brushWidth
            beizerPath.moveToPoint(points[0])
            beizerPath.addLineToPoint(points[0])
            beizerPath.lineCapStyle = .Round
            // localPath.lineJoinStyle = .Round
            //beizerPath.lineWidth = brushWidth
            colorChange()
        }
        
        self.drawBitmapImage()
        self.setNeedsDisplay()
        beizerPath.removeAllPoints()
      
        
        
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?)  {
        self.touchesEnded(touches!, withEvent: event)
    }
    
    
    
      // MARK: -  Drawing
    
    
     func drawBitmapImage() {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0);
        if incrImage != nil {
            let rectpath = UIBezierPath(rect: self.bounds)
            backgroundColorChange()
            //   UIColor.clearColor().setFill()
            rectpath.fill()
        }
        incrImage?.drawAtPoint(CGPointZero)
        
        colorChange()
        beizerPath.stroke()
        incrImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    
    
    // MARK: - clean the canvas
    func erase() {
        beizerPath.removeAllPoints()
        beizerPath = UIBezierPath()
        backgroundColorChange()
        incrImage = nil
        self.setNeedsDisplay()
    }
    
    
 // MARK: - eraser
    func colorChange(){
        if !eraser {
            let color : UIColor = UIColor(red: red/255, green: green/255, blue: blue/255, alpha: opacity)
            color.setStroke()
            color.setFill()
        } else {
            let color : UIColor = bgColor
            color.setStroke()
            color.setFill()
        }
        
  }
    

    
// MARK: - backgroungColorChange
    func  backgroundColorChange() {
        if(initialBG==0){
            UIColor.clearColor().setFill()
        }else {
            let color : UIColor = bgColor
            self.backgroundColor = bgColor
            color.setFill()
        }
    }
    
    
}


