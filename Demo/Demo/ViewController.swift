//
//  ViewController.swift
//  Demo
//
//  Created by Radhika on 8/17/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit


class ViewController: UIViewController, UIPopoverPresentationControllerDelegate, TableViewColorDelegate {
    
    
    @IBOutlet weak var drawingContainer: UIView!
    var signatureView : DrawView?
    var drawImage : UIImage?
    
    
    @IBAction func tapClear(sender: AnyObject) {
        
        signatureView?.erase()
    }
    
    
    @IBAction func tapSave(sender: AnyObject) {
        
        
        if (signatureView?.incrImage==nil) {
            
            let alertController = UIAlertController(title: "Save Image", message: "Its empty!", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }else{
        
            let activityViewController = UIActivityViewController(activityItems: [(signatureView?.incrImage)!], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = sender as! UIButton // so that iPads won't crash
            activityViewController.excludedActivityTypes = [ UIActivityTypeAirDrop, UIActivityTypePostToFacebook ]
            // present the view controller
            self.presentViewController(activityViewController, animated: true, completion: nil)
            UIGraphicsEndImageContext()
        }
        
}

    
    @IBAction func eraserPressed(sender: AnyObject) {
        
        let  eraser  = sender as! UIButton
       
        if(!((signatureView?.eraser)!)){
        signatureView?.eraser = true
        eraser.selected = true
        }else {
            signatureView?.eraser = false
            eraser.selected = false
        }
    }
    
    
    @IBAction func pencilPressed(sender: AnyObject) {
        
        let tableViewController = DropdownViewController()
        tableViewController.delegate = self
        tableViewController.modalPresentationStyle = UIModalPresentationStyle.Popover
        tableViewController.preferredContentSize = CGSizeMake(200, 200)
        presentViewController(tableViewController, animated: true, completion: nil)
        let popoverPresentationController = tableViewController.popoverPresentationController
        popoverPresentationController?.sourceView = sender as! UIButton
        popoverPresentationController?.sourceRect = CGRectMake(0, 0, sender.frame.size.width, sender.frame.size.height)
        
    }
    
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController!) -> UIModalPresentationStyle {
        return .None
    }
    
    
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
       // super.viewDidLoad()
    }
    
     override func didReceiveMemoryWarning() {
      //  super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    override func viewDidAppear(animated: Bool) {
       signatureView = DrawView(frame: CGRectMake(0, 0, drawingContainer.bounds.size.width,  drawingContainer.bounds.size.height))
      drawingContainer.addSubview(signatureView!)
   }
    
    
    
    // MARK: - TableViewDelegate Methods
    
    func didSelectBackgroundColor(r: CGFloat, g: CGFloat, b:CGFloat)
    {
        signatureView?.initialBG = 1
        signatureView?.bgColor = UIColor(red: r/255, green : g/255, blue: b/255, alpha: 1)
    }
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let settingsViewController = segue.destinationViewController as! SettingsViewController
        settingsViewController.delegate = self
        settingsViewController.opacity = (signatureView?.opacity)!
        settingsViewController.red = (signatureView?.red)!
        settingsViewController.green = (signatureView?.green)!
        settingsViewController.blue = (signatureView?.blue)!
    }
}



  // MARK: -  SettingsViewControlDelegate

    extension ViewController: SettingsViewControllerDelegate {
        func settingsViewControllerFinished(settingsViewController: SettingsViewController) {
        signatureView!.brushWidth = settingsViewController.brush
        signatureView!.opacity = settingsViewController.opacity
        signatureView!.red = settingsViewController.red
        signatureView!.green = settingsViewController.green
        signatureView!.blue = settingsViewController.blue
    }
}







