//
//  ViewController.swift
//  Push
//
//  Created by Radhika on 8/2/16.
//  Copyright © 2016 Radhika. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

   
    @IBOutlet weak var fieldA: UITextField!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
//    @IBAction func btnSubmit(sender: UIButton) {
//        
//        self.performSegueWithIdentifier("gotoSecond", sender: self)
//    
//    }

  override  func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "gotoSecond") {
            let svc : SecondViewController = segue.destinationViewController as! SecondViewController
            
            if let text = fieldA.text{
                
               svc.text = text
                
            }
            
            
        }
    }
   override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
}


}

