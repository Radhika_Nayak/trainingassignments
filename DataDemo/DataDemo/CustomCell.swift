//
//  CustomCell.swift
//  DataDemo
//
//  Created by Radhika on 8/10/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit

class CustomCell: UICollectionViewCell {

    @IBOutlet weak var firstName: UITextField!
    
    @IBOutlet weak var lastName: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
