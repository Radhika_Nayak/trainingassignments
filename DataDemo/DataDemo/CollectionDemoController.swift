//
//  CollectionDemoController.swift
//  DataDemo
//
//  Created by Radhika on 8/10/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit
import CoreData

class CollectionDemoController: UIViewController , UICollectionViewDataSource{
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var people1 = [NSManagedObject]()


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        title = "\"Employee List\""
       
        
        collectionView.registerNib(UINib(nibName: "CustomCell", bundle:nil), forCellWithReuseIdentifier: "cellId")
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
      func collectionView(collectionView: UICollectionView,
                                numberOfItemsInSection section: Int) -> Int {
        return people1.count
    }
    
   func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cellId", forIndexPath: indexPath) as! CustomCell
    
        // Configure the cell
    
    

       let person = people1[indexPath.row]
    
    // Configure Cell
        cell.firstName.text =   person.valueForKey("firstName") as? String
    
        cell.firstName.backgroundColor = UIColor.grayColor()
        cell.lastName.text = person.valueForKey("lastName") as? String
    
        return cell
    }
 
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width:260, height: 128)
    }

    
    
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Employee")
        let yearSort = NSSortDescriptor(key : "firstName", ascending: true)
        fetchRequest.sortDescriptors = [yearSort]
        
        do {
            let results =
                try managedContext.executeFetchRequest(fetchRequest)
            people1 = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
    }
    
    
}