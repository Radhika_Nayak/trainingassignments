//
//  SecondViewController.swift
//  DataDemo
//
//  Created by Radhika on 8/9/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit
import CoreData

class SecondViewController: UIViewController, UITableViewDataSource {

  //  var fetchedResultsController: NSFetchedResultsController!
    var people1 = [NSManagedObject]()

    
    @IBOutlet weak var tableView: UITableView!
    
    
    //let secondVC = storyBoard.instantiateViewControllerWithIdentifier("ViewController") as! ViewController

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "\"Employee List\""
        tableView.registerClass(UITableViewCell.self,
                                forCellReuseIdentifier: "Cell")
        
      //  self.tableView.reloadData()
        
        
           
        


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
   func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
 func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return people1.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
     
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
            
            // Fetch Fruit
            let person = people1[indexPath.row]
            
            // Configure Cell
            cell.textLabel!.text =
            person.valueForKey("firstName") as? String
           return cell
}
    
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Employee")
        let yearSort = NSSortDescriptor(key : "firstName", ascending: true)
        fetchRequest.sortDescriptors = [yearSort]
        
        do {
            let results =
                try managedContext.executeFetchRequest(fetchRequest)
            people1 = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }

    }
    
    
    
    
}