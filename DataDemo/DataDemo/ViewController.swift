//
//  ViewController.swift
//  DataDemo
//
//  Created by Radhika on 8/9/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    
  var people = [NSManagedObject]()
    
    
    @IBOutlet weak var displayId: UITextField!
    @IBOutlet weak var displayFirstName: UITextField!
    @IBOutlet weak var displayLastName: UITextField!
    @IBOutlet weak var displayDOB: UITextField!
    
    
    
   
     @IBAction func displayAction(sender: AnyObject) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let secondVC = storyBoard.instantiateViewControllerWithIdentifier("SecondViewController") as! SecondViewController
            secondVC.people1 = people

        navigationController?.pushViewController(secondVC, animated: true)
    }
    
    
    
    
    @IBAction func displayCollection(sender: AnyObject) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let thirdVC = storyBoard.instantiateViewControllerWithIdentifier("CollectionDemoController") as! CollectionDemoController
        thirdVC.people1 = people
        
        navigationController?.pushViewController(thirdVC, animated: true)
        
        
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
       title = "\"Add Employee Details \""
        
    }
    
    
        

        
    @IBAction func addDetails(sender: AnyObject) {
    
            
        
        
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
      
        let entity =  NSEntityDescription.entityForName("Employee",
                                                        inManagedObjectContext:managedContext)
        
        let employee = NSManagedObject(entity: entity!,
                                     insertIntoManagedObjectContext: managedContext)
        
    
        employee.setValue(displayFirstName.text, forKey: "firstName")
        employee.setValue(displayLastName.text, forKey: "lastName")

        employee.setValue(displayDOB.text, forKey: "dob")

        employee.setValue(displayId.text, forKey: "id")
        
       
        do {
            try managedContext.save()
          
            people.append(employee)
            
            people.sortInPlace { ($0.valueForKey("id") as! String) < ($1.valueForKey("id") as! String) }
            
            
            print(people)
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }

        
        
    }
        
        
    @IBAction func clearFields(sender: AnyObject) {
        
        displayId.text = ""
        displayFirstName.text = ""
        displayLastName.text = ""
        displayDOB.text = ""
        
    }
    
        
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
//    
//    @IBAction func cancelButton(sender: AnyObject) {
//        self.navigationController?.popToRootViewControllerAnimated(true)
//    }

}

