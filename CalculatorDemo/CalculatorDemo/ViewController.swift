//
//  ViewController.swift
//  CalculatorDemo
//
//  Created by Radhika on 8/3/16.
//  Copyright © 2016 Radhika. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    
    @IBOutlet weak var displayText: UILabel!
    var value1 : String = ""
    var value3 : String = ""
    var op1 : String = ""
    var op2 : String = ""
    var temp : String = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func tapoperand(sender: UIButton) {
        
       
        
        let buttonString = (sender.titleLabel?.text)!
        
        displayText.text = displayText.text! + buttonString
        
   
}
    
    
    
    @IBAction func tapOperator(sender: UIButton) {
        
        
        let buttonString = sender.titleLabel?.text
        value1 = displayText.text!
        op1=value1
        
        
        
        displayText.text =  buttonString
    
        value3.appendContentsOf(displayText.text!)
        
        
       displayText.text = ""
        
    }
    
    
    
    
    
    @IBAction func tapClear(sender: UIButton) {
        
        
          op1 = ""
          op2 = ""
          value1=""
          displayText.text=""
          value3 = ""
          temp = ""
}
    
    
    
    @IBAction func tapequals(sender: AnyObject) {
        
        
        var resultvalue = 0.0
        
        let operation = value3
        
         value1 = displayText.text!
         op2 = value1
         switch operation{
            
         case  "+"  : resultvalue = Double(op1)! + Double(op2)!
         case  "-"  : resultvalue =  Double(op1)! -  Double(op2)!
         case  "*"  : resultvalue =  Double(op1)! * Double(op2)!
         case  "/"  :resultvalue =  Double(op1)! / Double(op2)!
         default  : resultvalue = 0
            
        }
        
        
        displayText.text = String(resultvalue)
       
        
    }
    
        
        
        
    
}

