//
//  CustomCell.swift
//  Table
//
//  Created by Radhika on 8/8/16.
//  Copyright © 2016 Radhika. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

    @IBOutlet weak var displayimage: UIImageView!
    @IBOutlet weak var display: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
