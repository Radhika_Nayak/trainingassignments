//
//  DataTableViewController.swift
//  Table
//
//  Created by Radhika on 8/5/16.
//  Copyright © 2016 Radhika. All rights reserved.
//
import UIKit

class DataTableViewController: UITableViewController {
    
    override func viewDidLoad() {
     tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "LabelCell")
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 1
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Section \(section)"
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("LabelCell", forIndexPath: indexPath)
        
        cell.textLabel?.text = "Section \(indexPath.section) Row \(indexPath.row)"
        
        return cell
    }
   
    
}
