//
//  ScrollViewImageViewController.swift
//  Table
//
//  Created by Radhika on 8/8/16.
//  Copyright © 2016 Radhika. All rights reserved.
//

import UIKit

class ScrollViewImageViewController: UIViewController {

   
    
    
    @IBOutlet weak var displayImage: UIScrollView!

    var imageArray : [String] = ["Image-1","Image-2","Image"]
    var imageViews:[UIImageView] = []
    
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
       displayImage.pagingEnabled = true
       displayImage.showsVerticalScrollIndicator = true
        
        
        
        for(var i = 0;i<imageArray.count;i += 1){
            let imageView = UIImageView(image: UIImage(named : imageArray[i]))
            imageView.frame = CGRectMake(0 + (CGFloat(i) * displayImage.bounds.size.width),0, (displayImage.bounds.size.width - 20), (displayImage.bounds.size.height - 20))
            displayImage.addSubview(imageView)
            
            imageViews.append(imageView)
            
        }
        displayImage.contentSize = CGSize(width: 4 * displayImage.frame.size.width , height: displayImage.frame.size.height)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    
    @IBAction func TapBackButton(sender: AnyObject)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
